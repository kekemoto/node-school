"use strict"
var express = require('express')
var fs = require('fs')
var app = express()

app.get("/books", function(request,response){
	fs.readFile(process.argv[3], function(err, data){
		if(err) console.error(err)
		response.json(JSON.parse(data.toString()))
	})
})
app.listen(process.argv[2])
