"use strict"
var bodyParser = require('body-parser')
var express = require('express')
var app = express()

app.use (bodyParser.urlencoded({extended: false}))

app.post('/form', function(request, response){
	response.send(request.body.str.split('').reverse().join(''))
})

app.listen(process.argv[2])
