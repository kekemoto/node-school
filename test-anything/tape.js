"use strict"
var fancify = require(process.argv[2])
var test = require('tape')

test('fancyfy wrappend input', function(t){
	t.ok(fancify('Hello') === '~*~Hello~*~')
	t.ok(fancify('Hello', true) === '~*~HELLO~*~')
	t.ok(fancify('Hello', false, '!') === '~!~Hello~!~')
	t.end()
})

/*
 *test('fancify', function (t) {
 *	t.equal(fancify('Wat'), '~*~Wat~*~', 'Wraps a string in ~*~')
 *	t.equal(fancify('Wat', true), '~*~WAT~*~', 'Optionally makes it allcaps')
 *	t.equal(fancify('Wat', false, '%'), '~%~Wat~%~', 'Optionally allows to set the character')
 *	t.end()
 *})
 */
