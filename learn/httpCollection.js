
var http = require("http");

http.get(process.argv[2], function(response){
	var stack = ""
	response.setEncoding("utf8")
	response.on("data", data => {
		stack += data;
	})
	response.on("end", () => {
		console.log(stack.length)
		console.log(stack)
	})
	response.on("error", console.error)
}).on("error", console.error)

/*
var http = require('http')  
     var bl = require('bl')  
       
     http.get(process.argv[2], function (response) {  
       response.pipe(bl(function (err, data) {  
         if (err)  
           return console.error(err)  
         data = data.toString()  
         console.log(data.length)  
         console.log(data)  
       }))    
     })
*/
