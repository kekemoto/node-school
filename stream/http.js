"use strict"
var http = require('http')
var through = require('through')

http.createServer((request, response)=>{
	request.pipe(through(function(chunk){
		this.push(chunk.toString().toUpperCase())
	})).pipe(response)
}).listen(process.argv[2])
