"use strict";
var fs = require("fs");
var path = require("path");

fs.readdir(process.argv[2],function(err,data){
	if(err) throw err;
	var extension = "." + process.argv[3];
	for(let value of data){
		if(extension == path.extname(value)){
			console.log(value);
		}
	}
});
