"use strict";
var http = require("http")
var bl = require("bl")
var urlList = process.argv.slice(2)

for (let i=0; i<urlList.length; i++){
	http.get(urlList[i], (response) => {
		response.pipe(bl((err,data) => {
			if(err) return console.error(err)
			endCheck(i, data.toString(), (list) => {
				list.forEach((item,index)=>{console.log(item)})
			})

		}))
		response.on("error", console.error)
	})
}

function makeClosure(length){
	var complete = []
	var count = 0
	return function(index, data, callback){
		complete[index] = data
		count++
		if(count === length)
			callback(complete)
	}
}
var endCheck = makeClosure(urlList.length)
