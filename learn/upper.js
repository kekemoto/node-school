"use strict"
var http = require('http')
var map = require('through2-map')

var server = http.createServer((request,response)=>{
	request.pipe(map(chunk=>{
		return chunk.toString().toUpperCase()
	})).pipe(response)
})
server.listen(process.argv[2])
