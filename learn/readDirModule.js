"use strict";
var fs = require("fs");
var path = require("path");

//modularize this function that read folder and then filter on extension 
module.exports = function(folderPath, extension, callback){
	fs.readdir(folderPath,function(err,data){
		if(err) return callback(err)

		data = data.filter(function(file){
			return "." + extension === path.extname(file)
		})
		callback(null, data)
	})
}

/*
     module.exports = function (dir, filterStr, callback) {  
       
       fs.readdir(dir, function (err, list) {  
         if (err)  
           return callback(err)  
       
         list = list.filter(function (file) {  
           return path.extname(file) === '.' + filterStr  
         })  
       
         callback(null, list)  
       })  
     }  
*/
