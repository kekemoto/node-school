"use strict"

var url = require('url')
var http = require('http')

var server = http.createServer((request, response)=>{
	let  parsed = url.parse(request.url, true)
	let date = new Date(parsed.query.iso)
	response.writeHead(200, { "Content-Type" : "application/json" })
	if(parsed.pathname === "/api/parsetime"){
		let json = {
			"hour" : date.getHours(),
			"minute" : date.getMinutes(),
			"second" : date.getSeconds()
		}
		response.end(JSON.stringify(json, null, '\t'))
	}
	else if(parsed.pathname === "/api/unixtime"){
		let json = { "unixtime" : date.getTime() }
		response.end(JSON.stringify(json, null, '\t'))
	}
	else{
		console.error('4o4')
		response.end()
	}
})
server.listen(process.argv[2])

/*
var http = require('http')  
var url = require('url')  

function parsetime (time) {  
	return {  
		hour: time.getHours(),  
		minute: time.getMinutes(),  
		second: time.getSeconds()  
	}  
}  

function unixtime (time) {  
	return { unixtime : time.getTime() }  
}  

var server = http.createServer(function (req, res) {  
	var parsedUrl = url.parse(req.url, true)  
	var time = new Date(parsedUrl.query.iso)  
	var result  

	if (/^\/api\/parsetime/.test(req.url))  
		result = parsetime(time)  
	else if (/^\/api\/unixtime/.test(req.url))  
		result = unixtime(time)  

	if (result) {  
		res.writeHead(200, { 'Content-Type': 'application/json' })  
		res.end(JSON.stringify(result))  
	} else {  
		res.writeHead(404)  
		res.end()  
	}  
})  
server.listen(Number(process.argv[2]))  
*/
