"use strict"
var split = require('split')
var through = require('through2')
var th = through((function(){
	var toggle = 'odd'
	return function(line, encoding, next){
		if(toggle == "odd")
			this.push(line.toString().toLowerCase() + '\n')
		else
			this.push(line.toString().toUpperCase() + '\n')
		
		toggle=="odd" ? toggle="even" : toggle="odd"
		next()
	}
})())

process.stdin.pipe(split()).pipe(th).pipe(process.stdout)
