"use strict"
var repeatCallback = require(process.argv[2])
var test = require('tape')

test('repeatCallback',function(t){
	t.plan(2)
	repeatCallback(2, function(){
		t.pass('callback test')
	})
})


/*
 *test('fancify', function (t) {
 *	t.equal(fancify('Wat'), '~*~Wat~*~', 'Wraps a string in ~*~')
 *	t.equal(fancify('Wat', true), '~*~WAT~*~', 'Optionally makes it allcaps')
 *	t.equal(fancify('Wat', false, '%'), '~%~Wat~%~', 'Optionally allows to set the character')
 *	t.end()
 *})
 */
